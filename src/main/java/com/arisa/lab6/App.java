package com.arisa.lab6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank arisa = new BookBank("Arisa" , 50.0); // Constructor

        arisa.print();
        BookBank prayud = new BookBank("Prayudd", 100000.0);
        prayud.print();
        prayud.withdraw(40000.0) ;
        prayud.print();

        arisa.deposit(40000.0);
        arisa.print();

        BookBank prawit = new BookBank("Prawit" , 1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
