package com.arisa.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }


    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }
    
    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldCreateRobotUpSuccess1(){
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotUpSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }
    @Test
    public void shouldRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    
    @Test
    public void shouldRobotDownNSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(3);
        assertEquals(true, result);
        assertEquals(12, robot.getY());
    }

    @Test
    public void shouldRobotDownNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(12);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotLeftNSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 0);
        boolean result = robot.left(10);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotLeftNFail() {
        Robot robot = new Robot("Robot", 'R', 3, 0);
        boolean result = robot.left(4);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotRightNSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 1);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(15, robot.getX());
    }

    @Test
    public void shouldRobotRightNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 1);
        boolean result = robot.right(10);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
    
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11) ;
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }
}

        
